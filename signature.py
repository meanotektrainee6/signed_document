#Модуль находит все поля с подписями на картинке и определяет подписан ли он

from neuthink import nImage as im  
import neuthink.metaimage as Image 
from PIL import ImageDraw
from neuthink.graph.basics import Graph,Node
#from search_for_signature.is_handwriting_aug import  is_handwriting_aug as is_handwriting2
#from search_for_signature.is_handwriting2 import  is_handwriting2 as is_handwriting2
from search_for_signature.is_handwriting import  is_handwriting as is_handwriting2
from math import sqrt, pow
from typing import Union, List, Dict, Tuple
from PIL import Image as pilImage
from neuthink.graph.basics import Node
import pytesseract


def extend_image(original: pilImage, component: Node) -> pilImage:
    '''
        Возвращает расширенную картинку компонента

        Берет x0, y0, x1, y1 как значение по ключу location у component(формат значения ключа: ((x0, y0),(x1, y1)))
        y1 увеличивает на 5 единиц
        Обрезает original по координатам x0, y0, x1, y1
        Возвращает обрезанный рисунок
    '''
    pass


def is_signature(img: pilImage) -> bool:
    '''
        Определяет текст на рисунке, если на нем (не)написано 'signature', возвращает (False)True

        С помощью программы tesseract определяет, что написано на рисунке img
        Переводит в нижний регистр и удаляет пробелы  в начале и конце строк
        Возвращает True, если полученная строка равна 'signature', иначе - False

        >>> imt = pilImage.open('files/signature_word.png')
        >>> is_signature(imt)
        True
        >>> imt = pilImage.open('files/one_word.png')
        >>> is_signature(imt)
        False
    '''
    img_read = pytesseract.image_to_string(img).lower().strip() 

    return img_read == 'signature'

def get_distance(x0: int, y0: int, x1: int, y1: int) -> float:
    '''
        Вычисляет расстояние от точки x0, y0 до x1, y1

        Вычислить расстояние использую формулу √((x0- x1)^2 + (y0 - y1)^2)
        
        >>> get_distance(1, 1, 5, 6)
        6.4031242374328485
    '''
    return sqrt(pow(x0 - x1, 2) + pow(y0 - y1, 2))

def get_area(x0: int, y0: int, x1: int, y1: int) -> int:
    '''
        Вычисляет площадь прямоугольника по координатам верхней левой точки и нижней правой: 
        (x0, y0) и (x1, y1) соответственно

        Вычислить площадь использую формулу |x1- x0| * |y1 - y0|
        
        >>> get_area(1, 2, 5, 6)
        16
    '''
    return abs(x1 - x0) * abs(y1 - y0)

def get_near_components(cur_component: Node, components: List[Node]) -> List[Node]:
    '''
        Возвращает список компонентов из components, которые находятся на расстоянии не более 100 пикселей от компонента cur_component

        Получить кортеж с координатами cur_component взяв у него значение по ключу 'location'
        Присвоить переменным x и y значения элемента первого элемента полученного кортежа
        Пройтись по всем списку компонент, для каждого:
            Если значение по ключу id у cur_component не совпадает со значением id текущего элемента:
                Получить кортеж с координатами взяв у текущего элемента значение по ключу 'location'
                Присвоить переменным x_i и y_i значения элемента первого элемента полученного кортежа
                Найти расстояние между точками (x, y) и (x_i, y_i)  - get_distance
                Если расстояние меньше 100, то добавить текущий элемент в результирующий список
        Вернуть результирующий список

        >>> get_near_components({'id': 0, 'location' : ((0, 0), (1, 1))}, [{'id': 0, 'location' : ((0, 0), (1, 1))}, {'id': 1, 'location' : ((10, 10), (11, 11))}, {'id': 2, 'location' : ((100, 200), (110, 210))}])
        [{'id': 1, 'location': ((10, 10), (11, 11))}]
    '''
    x,y = cur_component['location'][0]
    result = []

    for component in components:
      x_i, y_i = component['location'][0] 

      if cur_component['id'] != component['id'] and get_distance(x, y, x_i, y_i) < 100:
        result += [component]

    return result        

def get_biggest_component(components: List[Node]) -> Node:
    '''
        Возвращает компонент с самой большой площадью

        Для каждого элемента из components:
            Взять в качестве значений координат ((x0, y0), (x1, y1)) значения по ключу 'location'
            Рассчитать площать компонента get_area(x0, y0, x1, y1)
        Вернуть компонент с наибольшей площадью, удовлетворяющий условиям:
            Высота прямоугольника меньше ширины, при том, что
            (x0, y0) - координаты верхней левой точки, (x1, y1) - нижней правой точки прямоугольника
            Минимальная ширина прямоугольника 30 пикселей, максимальная 200
            Минимальная высота прямоугольника 30 пикселей. максимальная 100

        >>> get_biggest_component([{'id': 0, 'location' : ((0, 0), (150, 80))}, {'id': 1, 'location' : ((10, 10), (11, 11))}, {'id': 2, 'location' : ((100, 200), (110, 210))}])
        {'id': 0, 'location': ((0, 0), (150, 80))}
    '''
    list_proper_components = []

    for i in components:
      (x0, y0), (x1, y1) = i['location']
      height = y1 - y0
      width = x1 - x0

      if 30 < height < 100 and 30 < width < 200 and height < width:
        list_proper_components += [i]

    areas = [get_area(i['location'][0][0], i['location'][0][1], i['location'][1][0],\
                      i['location'][1][1]) for i in list_proper_components] 
    index_comp = areas.index(max(areas))  

    return list_proper_components[index_comp]


def signed(image: im.mvImage) -> bool:
    '''
        Возвращает True, если на картинке есть рукописный код

        Переводит изобращение в монохромное
        Находит на изображении все компоненты: BasicSceneGraphParser
        Запускает функцию is_handwriting2 передавая список компонент
        Из полученного списка находит те узлы, у которых 'target_class': 'hand-hi', 
        но не '_state_delete': 'yes'
        Если такие узлы найдены, возвращает True, иначе - False

        >>> imgs = Image.Load('files/test/').Image.Mode().model
        >>> res = signed(imgs[0]['image'])
        2
        no split
        >>> res
        True
        >>> res = signed(imgs[1]['image'])
        9
        no split
        >>> res
        False
    '''
    image = image.Threshold(threshold=90)
    components = image.GetConnectedObjects(minsize=20)
    g = Graph()             
    n = Node(g,{'image':image})
    pnode = Image.BasicSceneGraphParser(n,components)  
    images = pnode.parent_graph.Match({'type':'image'})
    images = is_handwriting2(images)
    nodes = images.Match({'target_class':'hand-hi'}).NotMatch({'_state_delete': 'yes'})

    return len(nodes) > 0


def get_signature_field(original: pilImage, components: List[Node]) -> List[Node]:
    '''
        Из списка компонент связности находит те, что представляют из себя поле для подписи, возвращает в виде списка

        Пройтись по всему списку components, для каждого элемента (сomponent):
            Взять значение(image) по ключевому слову 'image', взять из него контент: сomponent['image'].content
            Вызвать extend_image с аргументами: original и сomponent['image'].content
            Вызвать is_signature, передав результат ф-ии extend_image
            Если результат вызова is_signature - True, то:
                Найти список ближайших компонент, который представляет собой поле для подписи: get_near_components
                передав в качестве первого аргумента текущий элемент сomponent и в качестве второго - components
                Если найденный список ближайших компонент не пустой:
                    Из полученного списка компонент найти самый большой по площади(biggest_component) - get_biggest_component,
                    в качетстве аргумента передать список, который вернула ф-я get_near_components
                    Получить изображение, взяв значение по ключу 'image' у узла biggest_component
                    Передать это изображение функции signed(), возвращаемое значение установить в качестве значения
                    ключа 'signed' у компонента с самой большой площадью - biggest_component
                    Добавить полученный элемент в результирующий список
        Вернуть полученный список компонент
    '''
    pass


if __name__ == "__main__":
    import doctest
    
    doctest.testmod()
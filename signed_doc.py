#Модуль для определения все ли поля для подпиcи в pdf документе заполнены
from PIL.PpmImagePlugin import PpmImageFile
from pdf2image import convert_from_path
from PIL import Image as pilImage
from neuthink.nImage  import mvImage
import neuthink.metaimage as Image 
from typing import List, Dict, Union, Tuple
import signature as sign 
import component as comp
from neuthink.graph.basics import Node
import os
    

def to_img(fileName: str) -> List[mvImage]:
    '''
        Возвращает список страниц в виде картинок в файле fileName

        Используя pdf2image конвертировать pdf файл fileName в список картинок
        Пройтись по полученному списку, фомируя новый список из элементов типа mvImage, конвертируя
        исходный элемент в mvImage: mvImage("",in_memory=True, source = page), где
        page - очередной элемент из списка картинок
        Вернуть новый список
        
        >>> len(to_img('files/2-in-one.pdf')) > 0
        True
    '''
    images = convert_from_path(fileName)
    list_result = []

    for page in images:
        list_result+=[mvImage("",in_memory=True, source = page)]
    
    return list_result    

def cut_pic(img: pilImage, first_half: bool) -> pilImage:
    '''
        Образает по высоте картинку пополам и возвращает первую половину, если first_half, иначе - вторую часть

        Получает оригинальные высоту и ширину картинки в пикселях(width, height = img.size)
        Вычисляет в пикселях половину высоты(half_height)
        Возвращает обрезанную картинку по высоте(img.crop(x0, y0, x1, y1)): 
        первую половину(x0 = 0, y0 = 0, x1 = width, y1 = half_height), если first_half, 
        иначе - вторую часть, нижнюю(x0 = 0, y0 = half_height, x1 = width, y1 = height)

        >>> orig_img = pilImage.open('files/OMWealth_full_list(2).jpg')
        >>> img2 = cut_pic(orig_img, False)
        >>> float(img2.size[1]) == orig_img.size[1] / 2
        True
    '''
    width, height = img.size
    half_height = height/2
    
    return img.crop((0, 0, width,half_height)) if first_half else img.crop((0, half_height, width, height))

def is_signed(components: List[Dict[str, Union[bool, Tuple[Tuple], mvImage, Dict[str, Union[mvImage, int]], str, int]]]) -> bool:
    '''
        Возвращает True, если все поля для подписи заполнены, иначе - False

        У каждого элемента из components проверяет значение по ключу 'signed'
        Если для всех элементов это значаение равняется True, возвращает True, 
        иначе - False
        Если список components пустой, вернуть True 

        >>> is_signed([{'id': 0, 'signed': True}, {'id': 1, 'signed': True}])
        True
        >>> is_signed([{'id': 0, 'signed': False}, {'id': 1, 'signed': True}])
        False
        >>> is_signed([])
        True
    '''
    for i in components:
      if i['signed']==False:
        return False

    return True   
               

def clean_temp_folder() -> None:
    '''
        Очищает каталог temp в текущем каталоге

        Проходит по всем файлам в папке temp и удаляет их        
        
        >>> clean_temp_folder() 
        >>> len(os.listdir('temp/'))
        0
    '''
    for filename in os.listdir('temp/'):
        os.remove('temp/' + filename)


def get_file_name(file_name: str) -> str:
    '''
        Возвращает название файла с расширением, но без пути к этому файлу

        Разделить file_name на элементы по символу '/', взять последний элемент

        >>> get_file_name('/manager/git.py')
        'git.py'
    '''
    return file_name.split('/')[-1]


def save_file(image: mvImage) -> str:
    '''
        Сохраняет файл image во временную папку temp, возвращает его полный путь с названием

        Получить количество файлов в папке temp
        Сохранить файл image в папку temp c названием: file(i).png,
        где i = количество файлов в папке temp
        Вернуть 'temp/' + file(i).png

        >>> orig_img = pilImage.open('files/OMWealth_full_list(2).jpg')
        >>> i = len(os.listdir('temp/'))
        >>> img = mvImage("",in_memory=True,source=orig_img)
        >>> res = save_file(img)
        >>> i == len(os.listdir('temp/')) - 1
        True
    '''
    ind = str(len(os.listdir('temp/')))
    new_name = os.path.join( 'file('+ind+').png' )
    image.content.save( 'temp/'+ new_name )
    
    return 'temp/'+ new_name


def get_clipped_imgs(imgs: List[mvImage]) -> List[mvImage]:
    '''
        Вовращает список картинок из исходного списка, поделенные пополам по высоте

        У каждой картинки получить вернюю половину и нижнюю: cut_pic,
        передав в качестве первого аргумента очередную картинку из изсходного списка картинок imgs
        в качестве второго True для получения первой половины картинки
        False - для получения второй половины картинки
        Каждую половинку помещать в результирующий список
        Вернуть этот список 

        >>> imgs = Image.Load('files/test/').Image.Mode().model
        >>> len(get_clipped_imgs([imgs[0]['image'], imgs[1]['image']])) == len(imgs) * 2
        True
    '''
    result_list = []

    for img in imgs:

        top_PIL = cut_pic(img.content,True)
        bottom_PIL = cut_pic(img.content,False)
        result_list += [mvImage("",in_memory=True, source = top_PIL)] +\
                       [mvImage("",in_memory=True, source = bottom_PIL)]
    return result_list         

def save_signature_fields(components: List[Node]) -> List[Dict[str, str]]:
    '''
        Сохраняет картинку каждого поля с подписью из components и вормирует словарь на основе исходного словаря

        Для каждого словаря (comp) из components составить новый словарь с ключами:
            'answer' со значением 'signed', если значение из comp по ключу 'signed' == True, иначе 'not signed'
            'image' - значение по ключу 'image'
            'coords' со значением по ключу 'location'
            'name' cо значением, возвращающим функцией save_file(image), где image - значение по ключу 'image'
    '''
    list_dict = []

    for comp in components:

        diction = {'coords':comp['location'],'image':comp['image'],'name':save_file(comp['image'])}
        diction['answer'] = 'signed' if comp['signed'] == True else 'not signed'
        list_dict+=[diction]

    return list_dict    


def get_signature_fields(file_name: str) -> List[Node]:
    '''
        Возвращает список полей с подписями в виде списка узлов в документе file_name

        Конвертирует pdf документ file_name в картинки - p.to_img
        Формирует список из обрезанных картинок - get_clipped_imgs, передав результат p.to_img
        Для каждой картинки из списка обрезанных:
            Получает список узлов - компонент - comp.get_connected_components, в качестве аргумента передав картинку
            Получает список узлов - компонент с подписями - sign.get_signature_field, в качестве аргументов:
            1. картинку, 2. список узлов - компонент
            Добавляет полученный список поэлементно в результирующий список
        Возвращает результирующий список
    '''
    list_imgs = get_clipped_imgs(to_img(file_name))
    list_Nodes =[]

    for img in list_imgs:
        components = comp.get_connected_components(img)
        list_Nodes += [sign.get_signature_field(img.content,components)]
    
    return list_Nodes     

def check_document(file_name: str) -> Dict[str, Union[bool, str, Dict[str, Union[bool, str]]]]:
    '''
        Находит в документе поля для подписи и проверяет их на наличие подписи,
        возвращает словарь с ключами: статус(подписан/нет), название документа, список полей для подписи

        Очистить временную папку - clean_temp_folder()
        Получить поля для подписи(components) - get_signature_fields(file_name)
        Сформировать словарь: 
        'result':'signature', 
        'answer' cо значением 'signed', если is_signed(components), иначе 'not signed'
        'file_name' со значением get_file_name(file_name)
        'blocks' cо значением, возвращаемым функцией save_signature_fields, с аргументом - список полей для подписи
    '''
    clean_temp_folder()

    components = get_signature_fields(file_name)
    dictionary_result = {'result':'signature','file_name':get_file_name(file_name),'blocks':save_signature_fields(components)}
    dictionary_result['answer'] = 'signed' if is_signed(components) else 'not signed'

    return dictionary_result


if __name__ == "__main__":
    import doctest

    doctest.testmod()

